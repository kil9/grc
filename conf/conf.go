# this configuration file is suitable for displaying kernel log files

# display this line in yellow and stop further processing
regexp=.*last message repeated \d+ times$
colours=yellow
count=stop
======
# date
regexp=^\d{4}\/\d{2}\/\d{2}
colours=green, green, red
count=once
======
# time
regexp=\d{2}:\d{2}:\d{2}
colours=yellow
count=once
======
# everything in `'
regexp=\`.+?\'
colours=bold yellow
count=more
======
# ip number
regexp=\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}
colours=bold magenta
count=more
======
# log level - info
regexp=\[(info|INFO)\]
colours=blue
count=more
======
# log level - warning
regexp=\[(warning|WARNING|warn|WARN)\]
colours=on_magenta
count=more
======
# detector
regexp=roi.*answer:.*
colours=yellow
count=once
======
# adult meter
regexp=adult:\ \d.*
colours=red
count=more
======
# log level - error
regexp=\[(erro|ERRO)(r|R)?\]
colours=on_red
count=more
======
# log level - critical
regexp=\[(critical|CRITICAL)\]
colours=on_red
count=more
======
# url
regexp=http[s]?:[^ ]*
colours=green
count=more
======
# log level - debug messages
regexp=\[(debu|DEBU)(g|G)?\]
colours=gray
count=more
=======
# important notice
regexp=\*{3}.*\*{3}
colours=on_blue
count=once
=======
# detector
regexp=det(ector)?=[^;]*
colours=magenta
count=more
=======
# class
regexp=cl(ass|asses|s)=[^\/;]*
colours=bold magenta
count=more
=======
# category
regexp=category:.*
colours=bold magenta
count=more
=======
# ocr
regexp=OCR\ not\ required.*
colours=bold yellow
count=more
=======
# api
regexp=\[api .*\]
colours=yellow
count=more
=======
# card
regexp=card\ added.*
colours=bold yellow
count=more
=======
# classification
regexp=image\ is\ class.*
colours=bold cyan
count=more
=======
# classification
regexp=\"[a-z_\-]*\"(\ (response))
colours=bold magenta
count=more
=======
# classification
regexp=\"[a-z_\-]*\"(\ (request))
colours=yellow
count=more


# vim: ft=sh
